#!/usr/bin/env bash
##################################################
#         RUN SCRIPT FOR COMPILATION             #
##################################################
#
# Build Docker Image && Container
# and intitiate
if [[ $(which docker) && $(docker --version) ]]; then
    echo "Building... "
    docker-compose build && docker-compose up -d
else
    echo "Please Install Docker First."
fi

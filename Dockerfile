#######################################
#     DOCKER CONFIGURATION FILE       #
#######################################
#
######### BEGIN #########
# @doc {
#   "An inspection for [.env] the following 
#   [Dockerfile] will be initiate on 
#   execution of the command."
# };
#
ARG PHP_VERSION=${PHP_VERSION}
# Image Declaration 
FROM webdevops/php-nginx:${PHP_VERSION}
#
# Nginx Configuration, listening on port 8000, 
# default setted on port 80 [http] & 443 [https]
#
# Clone [10-location-root.conf] into [vhost.common.d]
COPY ./nginx/10-location-root.conf /opt/docker/etc/nginx/vhost.common.d
#
# Clone [vhost.conf] into [nginx/]
COPY ./nginx/vhost.conf /opt/docker/etc/nginx
#
# Listening on Port at runtime
EXPOSE 80
# EXPOSE 443
#
# Container execution command
CMD php -S 0.0.0.0:8000 -t public >& /dev/null
#
######### END #########
# Build Image into Docker
# docker build --no-cache -t pgeon/app-name .
# docker run -p 8000:80 -d pgeon/app-name